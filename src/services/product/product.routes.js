module.exports = (app) => {
  const {create} = require("./product.create.js")
  const {findAll} = require("./product.findAll.js")

  var router = require("express").Router()

  // Create a new product
  router.post("/", create)

  // // Retrieve all products
  router.get("/", findAll)

  // // Retrieve all published products
  // router.get("/published", products.findAllPublished);

  // // Retrieve a single product with id
  // router.get("/:id", products.findOne);

  // // Update a product with id
  // router.put("/:id", products.update);

  // // Delete a product with id
  // router.delete("/:id", products.delete);

  // // Create a new product
  // router.delete("/", products.deleteAll);

  app.use("/api/products", router)
}
