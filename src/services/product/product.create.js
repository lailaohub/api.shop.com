const db = require("../../models")
const Product = db.products

exports.create = (req, res) => {
  // Validate request
  console.log("req.body: ", req.body)
  if (!req.body.name) {
    res.status(400).send({message: "Content can not be empty!"})
    return
  }

  // Create a product
  const product = new Product({
    name: req.body.name,
    detail: req.body.detail,
    price: req.body.price
  })

  // Save product in the database
  product
    .save(product)
    .then((data) => {
      res.send(data)
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the product."
      })
    })
}
