module.exports = (mongoose) => {
  const Product = mongoose.model(
    "product",
    mongoose.Schema(
      {
        name: String,
        detail: String,
        price: Number
      },
      {timestamps: true}
    )
  )

  return Product
}
